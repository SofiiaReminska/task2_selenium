package com.epam.lab.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class SendAndVerifyMailTest {

    private static final String BASE_URL = "https://mail.google.com";
    private static final Logger LOG = LogManager.getLogger(SendAndVerifyMailTest.class);
    private static WebDriver driver;

    @BeforeMethod
    public static void before() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        LOG.info("Starting browser");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public static void SendMailWithVerificationTest() {
        LOG.info("Loading page {}", BASE_URL);
        driver.get(BASE_URL);

        logIn();

        sendMessage();

        String expectedSentTime = new SimpleDateFormat("HH:mm").format(new Date(System.currentTimeMillis()));

        navigateToSentFolder();

        verifySentMessage(expectedSentTime);
    }

    private static void verifySentMessage(String expectedSentTime) {
        LOG.info("Verify message is sent");
        final WebElement recepientNameElement = driver.findElement(By.xpath("//*[@role='main']//tr[@draggable]//span[@email and not(@name='я')]"));
        assertEquals(recepientNameElement.getText(), "sofii.remi");

        final WebElement subjectElement = driver.findElement(By.xpath("//*[@role='main']//tr[@draggable]//div[@role='link']//span[@data-thread-id]"));
        assertEquals(subjectElement.getText(), "Test Mail");

        final WebElement timeElement = driver.findElement(By.xpath("//*[@role='main']//tr[@draggable]//*[@role='gridcell']/span/span"));
        assertEquals(timeElement.getText(), expectedSentTime);
    }

    private static void navigateToSentFolder() {
        LOG.info("Navigate to sent folder");
        final WebElement sentFolderButton = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(
                        By.xpath("//*[contains(@href, '/#sent')]")));
        sentFolderButton.click();

        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.invisibilityOfElementLocated(
                        By.xpath("//tr[@role='tablist']")));
    }

    private static void sendMessage() {
        LOG.info("Send message");
        final WebElement writeMailButton = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@jscontroller]//div[@role='button']")));
        writeMailButton.click();

        final WebElement enterRecipientInput = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name='to']")));
        enterRecipientInput.sendKeys("sofii.remi@gmail.com");

        final WebElement subjectInput = driver.findElement(By.xpath("//*[@name='subjectbox']"));
        subjectInput.sendKeys("Test Mail");

        final WebElement bodyInput = driver.findElement(By.xpath("//*[@role='textbox']"));
        bodyInput.sendKeys("Hello World!");

        final WebElement sendButton = driver.findElement(By.xpath("//table[@role='group']//*[@role='button']"));
        sendButton.click();
    }

    private static void logIn() {
        LOG.info("Login to gmail.com");
        final WebElement enterMailInput = driver.findElement(By.xpath("//*[@type='email']"));
        enterMailInput.click();
        enterMailInput.sendKeys("sofii.test@gmail.com");

        final WebElement identifierNextButton = driver.findElement(By.id("identifierNext"));
        identifierNextButton.click();

        final WebElement enterPasswordInput = (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@type='password']")));

        enterPasswordInput.click();
        enterPasswordInput.sendKeys("QwErTyUi");

        final WebElement passwordNextButton = driver.findElement(By.id("passwordNext"));
        passwordNextButton.click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(passwordNextButton));
    }

    @AfterMethod
    public void after() {
        driver.quit();
    }
}
